package fabricadepokemons;

/**
 *
 * @author Maurício El Uri
 */
public class main {

    public static void main(String[] args) {
        System.out.println("** Preparando testes ...");
        long tempo[][] = new long[9][7];
        long aux[];

        Teste testeLigada1 = new Teste();
        aux = testeLigada1.testaLigada(10000);
//        aux = testeLigada1.testaLigada(100);
        System.arraycopy(aux, 0, tempo[0], 0, 7);

        Teste testeLigada2 = new Teste();
        aux = testeLigada2.testaLigada(100000);
//        aux = testeLigada2.testaLigada(100);
        System.arraycopy(aux, 0, tempo[1], 0, 7);

//        Teste testeLigada3 = new Teste();
//        aux = testeLigada3.testaLigada(1000000);
//        System.arraycopy(aux, 0, tempo[2], 0, 7);

        Teste testeLHS1 = new Teste();
        aux = testeLHS1.testaLinkedHashSet(10000);
//        aux = testeLHS1.testaLinkedHashSet(100);
        System.arraycopy(aux, 0, tempo[3], 0, 7);

        Teste testeLHS2 = new Teste();
        aux = testeLHS2.testaLinkedHashSet(100000);
//        aux = testeLHS2.testaLinkedHashSet(100);
        System.arraycopy(aux, 0, tempo[4], 0, 7);

//        Teste testeLHS3 = new Teste();
//        aux = testeLHS3.testaLinkedHashSet(1000000);
//        System.arraycopy(aux, 0, tempo[5], 0, 7);

        Teste testeMap1 = new Teste();
        aux = testeMap1.testaMap(10000);
//        aux = testeMap1.testaMap(100);
        System.arraycopy(aux, 0, tempo[6], 0, 7);

        Teste testeMap2 = new Teste();
        aux = testeMap2.testaMap(100000);
//        aux = testeMap2.testaMap(100);
        System.arraycopy(aux, 0, tempo[7], 0, 7);

//        Teste testeMap3 = new Teste();
//        aux = testeMap3.testaMap(1000000);
//        System.arraycopy(aux, 0, tempo[8], 0, 7);

        System.out.println("\n\n\n***RESULTADOS DOS TESTES***\n\n\n");

        for (int x = 0; x < 9; x++) {
            switch (x) {
                case 0:
                    System.out.println("\n\n**Teste LinkedList***");
                    System.out.println("10.000 pokemóns\n\n");
                    break;
                case 1:
                    System.out.println("\n\n**Teste LinkedList***");
                    System.out.println("100.000 pokemóns\n\n");
                    break;
                case 2:
                    System.out.println("\n\n**Teste LinkedList***");
                    System.out.println("1.000.000 pokemóns\n\n");
                    break;
                case 3:
                    System.out.println("\n\n**Teste LinkedHashSet***");
                    System.out.println("10.000 pokemóns\n\n");
                    break;
                case 4:
                    System.out.println("\n\n**Teste LinkedHashSet***");
                    System.out.println("100.000 pokemóns\n\n");
                    break;
                case 5:
                    System.out.println("\n\n**Teste LinkedHashSet***");
                    System.out.println("1.000.000 pokemóns\n\n");
                    break;
                case 6:
                    System.out.println("\n\n**Teste Map***");
                    System.out.println("10.000 pokemóns\n\n");
                    break;
                case 7:
                    System.out.println("\n\n**Teste Map***");
                    System.out.println("100.000 pokemóns\n\n");
                    break;
                case 8:
                    System.out.println("\n\n**Teste Map***");
                    System.out.println("1.000.000 pokemóns\n\n");
                    break;
            }
            for (int y = 0; y < 7; y++) {

                switch (y) {
                    case 0:
                        System.out.println("\nTempo para inserir todos os Pokémons na coleção:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                    case 1:
                        System.out.println("\nTempo para verificação dos Pokémons tipo fogo:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                    case 2:
                        System.out.println("\nTempo para remover os Pokémons tipo água:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                    case 3:
                        System.out.println("\nTempo para armazenar individualmente os Pokémons:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                    case 4:
                        System.out.println("\nTempo para recuperar dados binários dos Pokémons:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                    case 5:
                        System.out.println("\nTempo para armazenar a coleção de dados binários:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                    case 6:
                        System.out.println("\nTempo para recuperar a coleção de dados binários:");
                        System.out.println(((double) tempo[x][y] / 1000000) + " milisegundos");
                        break;
                }
            }
        }
    }
}
