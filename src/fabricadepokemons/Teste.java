package fabricadepokemons;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author Maurício El Uri
 */
public class Teste {

    private LinkedList<Pokemon> LinkedPokemons;
    private LinkedHashSet<Pokemon> LHSPokemons;
    private Map MapPokemons;
    private int fogoLinkedPokemons, fogoLHSPokemons, fogoMapPokemons;
    private long tempoInicial;

    public Teste() {
        LinkedPokemons = new LinkedList<>();
        LHSPokemons = new LinkedHashSet<>();
        MapPokemons = new HashMap();
        fogoLinkedPokemons = fogoLHSPokemons = fogoMapPokemons = 0;
        tempoInicial = System.nanoTime();
    }

    public long[] testaLigada(int num) {
//        long tempo;
        long testes[] = new long[7];
        System.out.println("\n\nTestando lista ligada...");
        testes[0] = insereLinkedList(num);
        testes[1] = verificaFogoLinkedList();
//        alfabeticaLinkedList();
        testes[2] = removeAguaLinkedList();
        testes[3] = insereTodosLinked();
        testes[4] = leTodosLinked();
        testes[5] = gravaLinkedList();
        testes[6] = leLinkedList();
//        tempo = (System.nanoTime() - tempoInicial);
        limpaBinarioLinked();
        return testes;
    }

    public long[] testaLinkedHashSet(int num) {
//        long tempo;
        long testes[] = new long[7];
        System.out.println("\n\nTestando LinkedHashSet...");
        testes[0] = insereLinkedHashSet(num);
        testes[1] = verificaFogoLHSPokemons();
//        alfabeticaLinkedHashSet();
        testes[2] = removeAguaLHSPokemons();
        testes[3] = insereTodosLinkedHashSet();
        testes[4] = leTodosLinkedHashSet();
        testes[5] = insereLHS();
        testes[6] = leLHS();
//        tempo = (System.nanoTime() - tempoInicial);
        limpaBinarioLHS();
        return testes;
    }

    public long[] testaMap(int num) {
//        long tempo;
        long testes[] = new long[7];
        System.out.println("\n\nTestando HashMap...");
        testes[0] = insereHashMap(num);
        testes[1] = verificafogoMapPokemons(num);
//        alfabeticaMap();
        testes[2] = removeAguaMapPokemons(num);
        testes[3] = insereTodosMap(num);
        testes[4] = leTodosMap(num);
        testes[5] = insereMap();
        testes[6] = leMap();
//        tempo = (System.nanoTime() - tempoInicial);
        limpaBinarioMap(num);
        return testes;
    }

//    ================================================
//    INSERE POKEMONS NAS LISTAS
//    ================================================        
    public long insereLinkedList(int qtd) {
        long inicial = (System.nanoTime());
        while (LinkedPokemons.size() < qtd) {
            Pokemon poke = new Pokemon();
            LinkedPokemons.add(poke);
        }
        System.out.println("Linked size =  " + LinkedPokemons.size());
        return System.nanoTime() - inicial;
    }

    public long insereLinkedHashSet(int qtd) {
        long inicial = (System.nanoTime());
        while (LHSPokemons.size() < qtd) {
            Pokemon poke = new Pokemon();
            LHSPokemons.add(poke);
        }
        System.out.println("Hashset size = " + LHSPokemons.size());
        return System.nanoTime() - inicial;
    }

    public long insereHashMap(int qtd) {
        long inicial = (System.nanoTime());
        int x = 0;
        while (MapPokemons.size() < qtd) {
            Pokemon poke = new Pokemon();
            MapPokemons.put(x, poke);
            x++;
        }
        System.out.println("HashMap size = " + MapPokemons.size());
        return System.nanoTime() - inicial;
    }

//    ================================================
//    VERIFICA POKEMÓNS DO TIPO FOGO
//    ================================================
    public long verificaFogoLinkedList() {
        long inicial = (System.nanoTime());
        for (Pokemon poke : LinkedPokemons) {
            if (poke.getTipo().trim().toLowerCase().contains("fogo")) {
                fogoLinkedPokemons++;
            }
        }
        System.out.println("Pokemóns do tipo fogo: " + fogoLinkedPokemons);
        return System.nanoTime() - inicial;
    }

    public long verificaFogoLHSPokemons() {
        long inicial = (System.nanoTime());
        for (Pokemon poke : LHSPokemons) {
            if (poke.getTipo().trim().toLowerCase().contains("fogo")) {
                fogoLHSPokemons++;
            }
        }
        System.out.println("Pokemóns do tipo fogo: " + fogoLHSPokemons);
        return System.nanoTime() - inicial;
    }

    public long verificafogoMapPokemons(int num) {
        long inicial = (System.nanoTime());
        for (int x = 0; x < num; x++) {
            Pokemon poke = (Pokemon) MapPokemons.get(x);
            if (poke.getTipo().trim().toLowerCase().contains("fogo")) {
                fogoMapPokemons++;
            }
        }
        System.out.println("Pokemóns do tipo fogo: " + fogoMapPokemons);
        return System.nanoTime() - inicial;
    }

//    ================================================
//    EXIBE POKEMONS EM ORDEM ALFABÉTICA
//    ================================================
    public void alfabeticaLinkedList() {
        System.out.println("\n\nLINKEDLIST\n\n");
        LinkedList<Pokemon> pokemons = new LinkedList<>();
        pokemons.addAll(LinkedPokemons);
        Collections.sort(pokemons);
        Iterator<Pokemon> iterator = pokemons.iterator();
        while (iterator.hasNext()) {
            Pokemon pk = iterator.next();
            System.out.println(pk);
        }
    }

    public void alfabeticaLinkedHashSet() {
        System.out.println("\n\nLINKEDHASHSET\n\n");
        TreeSet<Pokemon> ordenado = new TreeSet();
        ordenado.addAll(LHSPokemons);
        Iterator<Pokemon> iterator = ordenado.iterator();
        while (iterator.hasNext()) {
            Pokemon pok = iterator.next();
            System.out.println(pok);
        }
    }

    public void alfabeticaMap() {
        System.out.println("\n\nMAP\n\n");
        SortedSet<Pokemon> values = new TreeSet<Pokemon>(MapPokemons.values());
        Iterator<Pokemon> iterator = values.iterator();
        while (iterator.hasNext()) {
            Pokemon pok = iterator.next();
            System.out.println(pok);
        }
    }

//    ================================================
//    REMOVE POKEMONS TIPO ÁGUA DAS LISTAS
//    ================================================
    public long removeAguaLinkedList() {
        long inicial = (System.nanoTime());
        System.out.println("\n\nRemovendo pokemóns do tipo água...");
        int removidos = 0;
        Iterator<Pokemon> iterator = LinkedPokemons.iterator();
        while (iterator.hasNext()) {
            Pokemon pk = iterator.next();
            if (pk.getTipo().trim().toLowerCase().contains("agua")) {
                iterator.remove();
                removidos++;
            }
        }
        System.out.println("\n" + removidos + " pokemóns do tipo água removidos");
        return System.nanoTime() - inicial;
    }

    public long removeAguaLHSPokemons() {
        long inicial = (System.nanoTime());
        int removidos = 0;
        Iterator<Pokemon> iterator = LHSPokemons.iterator();
        while (iterator.hasNext()) {
            Pokemon poke = iterator.next();
            if (poke.getTipo().trim().toLowerCase().contains("agua")) {
                iterator.remove();
                removidos++;
            }
        }
        System.out.println("\n" + removidos + " pokemóns do tipo água removidos");
        return System.nanoTime() - inicial;
    }

    public long removeAguaMapPokemons(int num) {
        long inicial = (System.nanoTime());
        int removidos = 0;
        for (int x = 0; x < num; x++) {
            Pokemon poke = (Pokemon) MapPokemons.get(x);
            if (poke.getTipo().trim().toLowerCase().contains("fogo")) {
                MapPokemons.remove(x);
                removidos++;
            }
        }
        System.out.println("\n" + removidos + " pokemóns do tipo água removidos");
        return System.nanoTime() - inicial;
    }

    /**
     * Grava no arquivo, cujo nome é informado por parâmetro, todas informações
     * do banco.
     *
     * @param nomeArquivo Nome do arquivo onde as informações do banco serão
     * gravadas.
     * @throwsException Retorna a exceção caso ocorra.
     */
    public void gravar(String nomeArquivo, Object o) throws Exception {
        ObjectOutputStream output;
        nomeArquivo = System.getProperty("user.dir") + "/src/fabricadepokemons/binarios/" + nomeArquivo + ".bin";
        output = new ObjectOutputStream(new FileOutputStream(new File(nomeArquivo)));
        output.writeObject(o);
        output.close();
    }

    /**
     * Lê do arquivo, cujo nome é informado por parâmetro, todas informações do
     * banco.
     *
     * @param nomeArquivo Nome do arquivo onde as informações do banco serão
     * lidas.
     * @throwsException Retorna a exceção caso ocorra.
     */
    public Object ler(String nomeArquivo) throws Exception {
        ObjectInputStream input;
        nomeArquivo = System.getProperty("user.dir") + "/src/fabricadepokemons/binarios/"
                + nomeArquivo + ".bin";
        input = new ObjectInputStream(new FileInputStream(new File(nomeArquivo)));
        Object o = input.readObject();
        input.close();
        return o;
    }

//    ================================================
//    GRAVA POKEMONS EM ARQUIVOS BINÁRIOS
//    ================================================
    public long insereTodosLinked() {
        long inicial = (System.nanoTime());
        System.out.println("Inserindo todos os pokemóns em arquivos binários");
        int tamanho = LinkedPokemons.size();
        System.out.println("[--------------------]");
        Iterator<Pokemon> iterator = LinkedPokemons.iterator();
        int x = 0;
        while (iterator.hasNext()) {
            exibeTamanho(x, tamanho);
            Pokemon pk = iterator.next();
            try {
                gravar("Linked" + x, pk);
            } catch (Exception e) {
                System.out.println("Erro ao gravar pokemón");
            }
            x++;
        }
        System.out.println("[====================]");
        return System.nanoTime() - inicial;
    }

    public long insereTodosMap(int num) {
        long inicial = (System.nanoTime());
        System.out.println("Inserindo todos os pokemóns em arquivos binários");
        int tamanho = MapPokemons.size();
        System.out.println("[--------------------]");
        for (int y = 0; y < num; y++) {
            exibeTamanho(y, tamanho);
            Pokemon pk = (Pokemon) MapPokemons.get(y);
            try {
                gravar("Map" + y, pk);
            } catch (Exception e) {
                System.out.println("Erro ao gravar pokemón");
            }
        }
        System.out.println("[====================]");
        return System.nanoTime() - inicial;
    }

    public long insereTodosLinkedHashSet() {
        long inicial = (System.nanoTime());
        System.out.println("Inserindo todos os pokemóns em arquivos binários");
        int tamanho = LHSPokemons.size();
        System.out.println("[--------------------]");
        Iterator<Pokemon> iterator = LHSPokemons.iterator();
        int x = 0;
        while (iterator.hasNext()) {
            exibeTamanho(x, tamanho);
            Pokemon pk = iterator.next();
            try {
                gravar("LHS" + x, pk);
            } catch (Exception e) {
                System.out.println("Erro ao gravar pokemón");
            }
            x++;
        }
        System.out.println("[====================]");
        return System.nanoTime() - inicial;
    }

//    ================================================
//    LE TODOS OS POKEMONS DOS ARQUIVOS BINÁRIOS
//    ================================================
    public long leTodosLinked() {
        long inicial = (System.nanoTime());
        System.out.println("Lendo todos os pokemóns em arquivos binários");
        int tamanho = LinkedPokemons.size();
        System.out.println("[--------------------]");
        Iterator<Pokemon> iterator = LinkedPokemons.iterator();
        int x = 0;
        while (iterator.hasNext()) {
            iterator.next();
            exibeTamanho(x, tamanho);
            try {
                Pokemon poke = (Pokemon) ler("Linked" + x);
            } catch (Exception e) {
                System.err.println("Erro ao ler pokemón");
            }
            x++;
        }
        System.out.println("[====================]");
        return System.nanoTime() - inicial;
    }

    public long leTodosLinkedHashSet() {
        long inicial = (System.nanoTime());
        System.out.println("Lendo todos os pokemóns em arquivos binários");
        int tamanho = LHSPokemons.size();
        System.out.println("[--------------------]");
        Iterator<Pokemon> iterator = LHSPokemons.iterator();
        int x = 0;
        while (iterator.hasNext()) {
            iterator.next();
            exibeTamanho(x, tamanho);
            try {
                Pokemon poke = (Pokemon) ler("LHS" + x);
            } catch (Exception e) {
                System.err.println("Erro ao ler pokemón");
            }
            x++;
        }
        System.out.println("[====================]");
        return System.nanoTime() - inicial;
    }

    public long leTodosMap(int num) {
        long inicial = (System.nanoTime());
        System.out.println("Lendo todos os pokemóns em arquivos binários");
        int tamanho = LHSPokemons.size();
        System.out.println("[--------------------]");
        for (int y = 0; y < num; y++) {
            exibeTamanho(y, tamanho);
            try {
                Pokemon poke = (Pokemon) ler("Map" + y);
            } catch (Exception e) {
                System.out.println("Erro ao ler pokemón");
            }
        }
        System.out.println("[====================]");
        return System.nanoTime() - inicial;
    }

    
    
    /**
     * Método que serve para exibir ao usuário estatística de arquivos binários
     * que já foi inserida e lida. Feito pois costuma demorar com grandes
     * quantidades de arquivos
     *
     * @param x
     * @param tamanho
     */
    public void exibeTamanho(int x, int tamanho) {
        if (x == tamanho * 0.1) {
            System.out.println("[==------------------]");
        }
        if (x == tamanho * 0.2) {
            System.out.println("[====----------------]");
        }
        if (x == tamanho * 0.3) {
            System.out.println("[======--------------]");
        }
        if (x == tamanho * 0.4) {
            System.out.println("[========------------]");
        }
        if (x == tamanho * 0.5) {
            System.out.println("[==========----------]");
        }
        if (x == tamanho * 0.6) {
            System.out.println("[============--------]");
        }
        if (x == tamanho * 0.7) {
            System.out.println("[==============------]");
        }
        if (x == tamanho * 0.8) {
            System.out.println("[================----]");
        }
        if (x == tamanho * 0.9) {
            System.out.println("[==================--]");
        }
    }
    
//    ================================================
//    GRAVA AS LISTAS COMPLETAS EM ARQUIVOS BINÁRIOS
//    ================================================
    public long gravaLinkedList() {
        long inicial = (System.nanoTime());
        try {
            gravar("LinkedList", LinkedPokemons);
        } catch (Exception e) {
            System.out.println("Erro ao inserir lista");
        }
        return System.nanoTime() - inicial;
    }

    public long insereLHS() {
        long inicial = (System.nanoTime());
        try {
            gravar("LHS", LHSPokemons);
        } catch (Exception e) {
            System.out.println("Erro ao inserir lista");
        }
        return System.nanoTime() - inicial;
    }

    public long insereMap() {
        long inicial = (System.nanoTime());
        try {
            gravar("Map", MapPokemons);
        } catch (Exception e) {
            System.out.println("Erro ao inserir lista");
        }
        return System.nanoTime() - inicial;
    }

//    ================================================
//    LÊ AS LISTAS COMPLETAS EM ARQUIVOS BINÁRIOS
//    ================================================
    public long leLinkedList() {
        long inicial = (System.nanoTime());
        try {
            LinkedList<Pokemon> lista = (LinkedList) ler("LinkedList");
//            System.out.println("Pk" + lista.size());
        } catch (Exception e) {
            System.out.println("Erro ao ler lista");
        }
        return System.nanoTime() - inicial;
    }

    public long leLHS() {
        long inicial = (System.nanoTime());
        try {
            LinkedHashSet<Pokemon> lista = (LinkedHashSet) ler("LHS");
//            System.out.println("Pk" + lista.size());
        } catch (Exception e) {
            System.out.println("Erro ao ler lista");
        }
        return System.nanoTime() - inicial;
    }

    public long leMap() {
        long inicial = (System.nanoTime());
        try {
            Map lista = (HashMap) ler("Map");
//            System.out.println("Pk" + lista.size());
        } catch (Exception e) {
            System.out.println("Erro ao ler lista");
        }
        return System.nanoTime() - inicial;
    }

//    ================================================
//    (ADICIONAL) LIMPA ARQUIVOS BINÁRIOS UTILIZADOS NOS TESTES
//    ================================================
    public void limpaBinarioLinked() {
        Iterator<Pokemon> iterator = LinkedPokemons.iterator();
        int x = 0;
        while (iterator.hasNext()) {
            iterator.next();
            if (deletar("Linked" + x + ".bin") == false) {
                System.err.println("Erro ao limpar arquivos do teste");
            }
            x++;
        }
        if (deletar("LinkedList.bin") == false) {
            System.err.println("Erro ao limpar arquivos do teste");
        }
    }

    public void limpaBinarioLHS() {
        Iterator<Pokemon> iterator = LHSPokemons.iterator();
        int x = 0;
        while (iterator.hasNext()) {
            iterator.next();
            if (deletar("LHS" + x + ".bin") == false) {
                System.err.println("Erro ao limpar arquivos do teste");
            }
            x++;
        }
        if (deletar("LHS.bin") == false) {
            System.err.println("Erro ao limpar arquivos do teste");
        }
    }

    public void limpaBinarioMap(int num) {
        for (int y = 0; y < num; y++) {
            if (deletar("Map" + y + ".bin") == false) {
                System.err.println("Erro ao limpar arquivos do teste");
            }
        }
        if (deletar("Map.bin") == false) {
            System.err.println("Erro ao limpar arquivos do teste");
        }
    }

    /**
     * Deleta arquivo recebido por parâmetro que estiver dentro da pasta
     * binarios
     *
     * @param nome
     * @return
     */
    public boolean deletar(String nome) {
        boolean resultado;
        File f = new File(System.getProperty("user.dir") + "/src/fabricadepokemons/binarios/" + nome);
        try {
            f.createNewFile();
        } catch (Exception e) {
            System.err.println("Erro ao limpar arquivos do teste");
        }
        resultado = f.delete();
        return resultado;
    }
}
